__author__ = "Christopher Lowe"


from queue import PriorityQueue, Queue
import copy
from misc import assign_uid, add_to_queue
import config
import sys


class DataRoutingModule:
    """
    Model of the DRM software component, which identifies the data to be transferred
    via ISL during a satellite-satellite cross-link
    """
    def __init__(self, u):
        """
        :param u: ID of the satellite carrying out the routing assessment
        :param v: ID of the neighbouring satellite
        :param cs: Contact Schedule object, describing the downstream contact
            opportunities available for potential relay opportunities
        :param vcb: Virtual Combined Buffer object, containing an ordered set of packets
            to be routed. Packets may reside physically on neighbouring satellites,
            but are considered during the routing process
        :param nrm: Network Resource Model object, which defines the expected
            downstream resources on the nodes and edges
        """
        self.u = u
        self.dests = None
        self.nodes = None

        # A Route Table is maintained that holds an ordered sequence of routes from
        # each node (the "root") to each possible destination (not the root node). This
        # table is then used to select the best route for a bundle, depending on the
        # current custodian (root) and destination of that bundle.
        self.route_table = {}

        # Dict of all routes, with their UID as the key and route object as the value
        self.routes = {}

        # This is to hold the routes that are assigned to each bundle during the bundle
        # assignment process. The KW will be the bundle UID and the value is a list of
        # tuples, with each tuple representing the (nodeID, time) pairs that make up
        # the route
        self.routed_bundles = {}

        # packets for which no feasible route was found and should be dropped
        self.failed_bundles =[]

        # Dict (hash table) of Contact objects
        self.contact_plan = []
        self.contact_plan_hash = dict()

    def setup(self, dests, nodes):
        # Define the network attributes, including the set of nodes that could be
        # defined as destinations. This is necessary in order to know to which nodes
        # contact graphs should be built
        self.dests = set(dests)
        self.nodes = set(nodes)
        self.reset()

    def route_discovery(self, cp, nrm, next_node):
        """
        Build the Route Table by identifying the highest-value routes to each
        destination node. This process comprises sub-routines of:
        - Generating a list of Contact objects
        - Building a Contact Graph
        - Finding the k best routes to each destination
        :param cp: Contact Plan table (pandas dataframe)
        :param nrm: Network Resource Model (pandas dataframe)
        :return:
        """
        # Extract the contact information from the Contact Plan
        self._build_contact_plan(cp)

        # Find the lowest cost routes to each destination using Yen's algorithm
        self._build_route_tables(next_node)

    def bundle_assignment(self, vcb, neighbour):
        """
        For each bundle in the combined buffer, identify the route over which it should
        be sent and reduce the resources along each Contact in that route accordingly.
        Stop once all of the bundles have been assigned a route. Bundles for which a
        feasible route (i.e. one that ensures delivery before the bundle's expiry) is
        not available shall be dropped from the buffer.
        :return:
        """
        # The assignments list will eventually hold (Bundle, Route) tuples, which are
        # then used to identify the "next" hop and therefore if the bundle should be
        # scheduled for being sent. I.e. if the bundle is
        to_send = []
        to_drop = []

        for b in vcb.bundles:
            opt_route_val = sys.maxsize
            opt_route = None

            # FIXME This currently falls over because the destination for the bundle
            #  can be self, for which we of course don't have a set of routes.
            for d in b.destination:
                for route in self.route_table[b.custodian][d]:
                    # if the route is not of higher value than the current best route
                    if route.arrival_time > opt_route_val:
                        continue
                    # if the route has insufficient capacity to accommodate the bundle
                    if route.min_cap <= b.size:
                        continue
                    # If the route concludes later than the bundle expires
                    if route.arrival_time >= b.expiry:
                        break

                    opt_route_val = route.arrival_time
                    opt_route = route
                    self.routes[route.uid] = route
                    break

            # If a suitable route has been found, assign the bundle to that route and
            # virtually reduce the resources along each of the contacts.
            if opt_route:
                # If the bundle is one of ours, assign it to the optimal route
                if b.custodian == self.u:
                    self.routed_bundles[b.uid] = {
                            'route': tuple((h.to, h.t_start) for h in opt_route.hops),
                            'time': config.TIME
                        }
                    if opt_route.hops[0].to == neighbour:
                        to_send.append(b.uid)
                # decrease the resources on the route's Contacts
                for c in opt_route.hops:
                    c.capacity -= b.size

            # Otherwise no feasible route exists so we need to drop that bundle
            else:
                if b.custodian == self.u:
                    to_drop.append(b.uid)
                    self.failed_bundles.append({
                        'bundle': b.uid,
                        'time': config.TIME
                    })

        return to_send, to_drop

    def _build_contact_plan(self, cp):
        # Generate a Contact object for each row in the CP and add to a dict, starting
        # with the root contact. The table should be arranged in a graph format,
        # such that the Contacts are organised according to the sending node and time
        for idx, row in cp.iterrows():
            contact = Contact(
                row['sending'],
                row['receiving'],
                row['start'],
                row['end'],
                row['rate']
            )
            self.contact_plan.append(contact)
            if not self.contact_plan_hash.get(contact.frm):
                self.contact_plan_hash[contact.frm] = []
            self.contact_plan_hash[contact.frm].append(contact)
        return

    def _build_route_tables(self, next):
        """
        Builds the route table for each destination, which stores the k-lowest cost
        paths that reach the destination. This is achieved through implementation of
        Yen's algorithm using self as the source node and each possible destination
        node as the destination.
        :return:
        """
        k = 10
        for node in (self.u, next):
            self.route_table[node] = {}
            for d in [x for x in self.dests if x != node]:
                self.route_table[node][d] = []
                routes = self._yens_ksp(node, d, k)
                if routes:
                    self.route_table[node][d] = routes

        return

    def _yens_ksp(self, root_node, d, num_routes):
        """
        Find the k shortest paths between nodes s & d in the graph, g
        :param g: graph
        :param root: starting Contact going from/to source node (self)
        :param d: destination node
        :param num_routes: number of shortest paths to return
        :return:
        """
        routes = []
        potential_routes = []

        # Root contact is the connection to self that acts as the source vertex in the
        # Contact Graph
        root = Contact(root_node, root_node, config.TIME, sys.maxsize, sys.maxsize)
        root.arrival_time = config.TIME

        # reset contacts
        for contact in self.contact_plan:
            contact.clear_dijkstra_area()
            contact.clear_management_area()

        # Find the lowest cost path using Dijkstra
        route = self._dijkstra(root, d)
        if route is None:
            return route

        routes.append(route)
        routes[0].hops.insert(0, root)

        # TODO Find other k-1 routes
        for k in range(num_routes - 1):
            for spur_contact in routes[-1].hops[:-1]:

                # create root_path from root_contact until spur_contact
                spur_contact_index = routes[-1].hops.index(spur_contact)

                root_path = Route(routes[-1].hops[0])
                for hop in routes[-1].hops[1:spur_contact_index + 1]:
                    root_path.append(hop)

                # reset contacts
                for contact in self.contact_plan:
                    contact.clear_dijkstra_area()
                    contact.clear_management_area()

                # suppress all contacts in root_path except spur_contact
                for contact in root_path.hops[:-1]:
                    contact.suppressed = True

                # suppress all outgoing edges from spur_contact already covered by known routes
                for route in routes:
                    if root_path.hops == route.hops[0:(len(root_path.hops))]:
                        if route.hops[len(root_path.hops)] not in spur_contact.suppressed_next_hop:
                            spur_contact.suppressed_next_hop.append(
                                route.hops[len(root_path.hops)]
                            )

                # prepare spur_contact as root contact
                spur_contact.clear_dijkstra_area()
                spur_contact.arrival_time = root_path.arrival_time
                for hop in root_path.hops:  # add visited nodes to spur_contact
                    spur_contact.visited_nodes.append(hop.to)

                # try to find a spur_path with dijkstra
                spur_path = self._dijkstra(spur_contact, d)

                # if found store new route in potential_routes
                if spur_path:
                    total_path = Route(root_path.hops[0])
                    for hop in root_path.hops[1:]:  # append root_path
                        total_path.append(hop)
                    for hop in spur_path.hops:  # append spur_path
                        total_path.append(hop)
                    potential_routes.append(total_path)

            # if no more potential routes end search
            if not potential_routes:
                break

            # sort potential routes by arrival_time
            potential_routes.sort()

            # add best route to routes
            routes.append(potential_routes.pop(0))

        # remove root_contact from hops and refresh values
        for route in routes:
            # print ("routes:",route)
            route.hops.pop(0)
            # route.refresh_metrics()

        return routes

    def _dijkstra(self, root, dest):
        """
        Finds the lowest cost Route from the current node to a destination node
        :return:
        """
        # TODO Consider restricting which contacts are added to the CG, since for a
        #  long time horizon with many contacts, this could become unnecessarily large.
        # Set of contacts that have been visited during the contact plan search
        # unvisited = [c.uid for c in self.contacts]
        [c.clear_dijkstra_area() for c in self.contact_plan if c is not root]

        current = root

        # Pre-set the variables used to track the "optimal" route and set the cost of
        # the "best" route so far to be a big number
        route = None
        final_contact = None
        earliest_fin_arr_t = sys.maxsize

        # TODO Identify a situation when this would ever NOT be the case, seems redundant
        if current.to not in current.visited_nodes:
            current.visited_nodes.append(current.to)

        while True:
            for contact in self.contact_plan_hash[current.to]:
                if contact in current.suppressed_next_hop:
                    continue
                if contact.suppressed or contact.visited:
                    continue
                if contact.to in current.visited_nodes:
                    continue
                if contact.t_end <= current.arrival_time:
                    continue
                # TODO Check should this be <= min bundle size?
                if contact.capacity <= 0:
                    continue
                if current.frm == contact.to and current.to == contact.frm:
                    continue

                # Calculate arrival time (cost)
                # If the next contact begins before we could have arrived at the
                # current contact, set the arrival time to be the arrival at the
                # current plus the time to traverse the contact
                if contact.t_start < current.arrival_time:
                    arrvl_time = current.arrival_time + contact.owlt
                else:
                    arrvl_time = contact.t_start + contact.owlt

                # Update cost if better or equal and update other parameters in
                # next contact so that we assume we're coming from the current
                if arrvl_time <= contact.arrival_time:
                    contact.arrival_time = arrvl_time
                    contact.predecessor = current
                    contact.visited_nodes = current.visited_nodes[:]
                    contact.visited_nodes.append(contact.to)

                    # Mark if destination reached
                    if contact.to == dest and contact.arrival_time < \
                            earliest_fin_arr_t:
                        earliest_fin_arr_t = contact.arrival_time
                        final_contact = contact

            # This completes our assessment of the current contact
            current.visited = True

            # Determine best next contact among all in contact plan
            earliest_arr_t = sys.maxsize
            next_contact = None

            for contact in self.contact_plan:

                # Ignore visited
                if contact.visited:
                    continue

                # If we know there is another, better contact, continue
                if contact.arrival_time > earliest_fin_arr_t:
                    continue

                if contact.arrival_time < earliest_arr_t:
                    earliest_arr_t = contact.arrival_time
                    next_contact = contact

            if not next_contact:
                break

            current = next_contact

        # Done contact graph exploration, check and store new route
        if final_contact is not None:

            hops = []
            contact = final_contact
            while contact != root:
                hops.insert(0, contact)
                contact = contact.predecessor

            route = Route(hops[0])
            for hop in hops[1:]:
                route.append(hop)

        return route

    def reset(self):
        self.contact_plan = []
        self.contact_plan_hash = {}
        self.routes = {}
        self.failed_bundles = []
        self.routed_bundles = {}
        self.route_table = {}


class Route:
    def __init__(self, contact, parent=None):
        """
        A journey is an ordered sequence of contact events.

        The capacity of each contact, combined with the availability (and other factors
        such as the available storage capacity on board the receiving node) will
        dictate the feasibility of the journey for a particular bundle.

        :param u: current custodian (node) of the data packet
        :param t_birth: time at which the data was first uploaded
        """
        self.__uid = assign_uid(config.UID_JOURNEY)
        self.parent = parent

        # TODO should be creating these using a property setter, or at least via the
        #  parent
        if not parent:
            self._hops = []
            self._feasible = True
            self._min_cap = 0
            self._availability = 1.
            self._arrival_time = sys.maxsize

        self.append(contact)

    @property
    def uid(self):
        return self.__uid

    @property
    def hops(self):
        """
        Return the contacts that have been traversed in the Journey so far, including
        those in the parent journey
        :return:
        """
        if self.parent:
            return self.parent.hops + self._hops
        else:
            return self._hops

    @property
    def feasible(self):
        # TODO add in some checks here to make sure the route is actually feasible
        if self.parent:
            return all((self.parent.feasible, self._feasible))
        else:
            return self._feasible

    @property
    def availability(self):
        if self.parent:
            return self.parent.availabity * self._availability
        else:
            return self._availability

    @property
    def min_cap(self):
        return min([x.capacity for x in self.hops])

    @property
    def arrival_time(self):
        return max([c.t_start for c in self.hops])

    def append(self, contact):
        """
        Add a hop to the journey
        :return:
        """
        self._hops.append(contact)

    # OPERATOR OVERLOAD FOR SELECTION #
    # Less than = this route is better than the other (less costly)
    def __lt__(self, other_route):
        # 1st priority : arrival time
        if self.arrival_time < other_route.arrival_time:
            return True

        # 2nd: volume
        elif self.arrival_time == other_route.arrival_time:
            if self.min_cap > other_route.min_cap:
                return True

            # 3rd: confidence
            elif self.min_cap == other_route.min_cap:
                if self.availability >= other_route.availability:
                    return True

        return False

    def refresh_uid(self, uid_):
        self.__uid = assign_uid(uid_)
        self.uid_list.append(self.__uid)

    def __repr__(self):
        return "to:%s|via:%s(%03d,%03d)|arvl:%s|hops:%s|vol:%s|conf:%s|" % \
               (
                   self.hops[-1].to,
                   self.hops[0].to,
                   self.hops[0].t_start,
                   self.hops[0].t_end,
                   self.arrival_time,
                   len(self.hops),
                   self.min_cap,
                   self.availability
               )


class Contact:
    """
    Contact class representing the opportunity for data transfer from a sending node (
    u) to a receiving node (v)
    """
    def __init__(self, u, v, t1, t2, rate):
        self.__uid = assign_uid(config.UID_CONTACT)
        self.frm = u
        self.to = v
        self.t_start = t1
        self.t_end = t2
        self.rate = rate
        self.owlt = 0

        # Total volume of data that could be sent over a contact
        self.capacity = rate * (t2 - t1)

        # route management working area
        self.suppressed = False
        self.suppressed_next_hop = []

        # Route search attributes
        self.arrival_time = sys.maxsize
        self.visited = False
        self.visited_nodes = []
        self.predecessor = None

    @property
    def uid(self):
        return self.__uid

    def clear_dijkstra_area(self):
        self.arrival_time = sys.maxsize
        self.visited = False
        self.visited_nodes = []
        self.predecessor = None

    def clear_management_area(self):
        self.suppressed = False
        self.suppressed_next_hop = []

    def __repr__(self):

        # replace with inf
        if self.t_end == sys.maxsize:
            end = "inf"
        else:
            end = self.t_end

        # mav in %
        volume = 100 * self.capacity / ((self.t_end - self.t_start)*self.rate)

        return "%s->%s(%s-%s)[cap%d%%]" % (self.frm, self.to, self.t_start, end, volume)


# TODO delete this
class ContactGraph:
    def __init__(self, cp, s, d, max_time):
        self.cg = {}
        k = 0
        for contact in cp.hops:
            self.cg[contact.uid] = []
            next_hop_time = 0
            for next_hop in cp.hops[k + 1:]:
                next_hop_time = next_hop.t_start
                if contact.t_start + max_time < next_hop_time:
                    break
                if next_hop.u == contact.v:
                    self.cg[contact.uid].append(next_hop.uid)
            k += 1


class VirtualCombinedBuffer:
    """
    Object that combines packets from multiple buffers and sorts them into an ordered
    list ready for routing
    """
    def __init__(self):
        self.packets = list()

    def add_packets(self, packets_u, packets_v):
        # TODO This is a bit hacky at the moment, because I couldn't easily deepcopy
        #  the packets list, which is a list of PriorityQueues. As such, we're passing
        #  in the actual list of PQs from the node's buffer object, extracting the
        #  packets one by one, highest priority first, adding a deepcopy of them to the
        #  VCB and then putting them back into the PriorityQueue so that it's unchanged
        #  going forward. This is a lot of activity for what should be a simple operation
        temp_q = [PriorityQueue() for _ in range(max(len(packets_u), len(packets_v)))]
        for packets_n in (packets_u, packets_v):
            put_back = []
            for priority in range(len(packets_n)):
                while not packets_n[priority].empty():
                    p = packets_n[priority].get()
                    temp_q[priority].put(copy.deepcopy(p))
                    put_back.append(p)

            for p in put_back:
                packets_n[p[2].priority].put(p)

        for priority in range(len(temp_q)):
            while not temp_q[priority].empty():
                self.packets.append(temp_q[priority].get()[2])

    def extract(self, n=1):
        """
        Extract the n highest priority packets from the combined buffer
        :return:
        """
        if not self.packets:
            return None

        extracted = []
        k = 0
        while self.packets and k < n:
            extracted.append(self.packets.pop(0))
            k += 1
        if n == 1:
            return extracted[0]
        return extracted

    def reset(self):
        self.packets = list()


class ContactController:
    """
    The general process of forming a contact, sharing information, carrying out route
    planning and transferring data between nodes.
    Specific actions are carried out on the nodes involved, but this class takes care
    of the contact environment for the simulation
    """
    def __init__(self, u, v):
        """
        :param u: Node object
        :param v: Node object
        """
        self.u = u
        self.v = v

    def execute_contact(self):
        """
        Master method that handles the different elements involved in a node-node contact
        :return:
        """
        self._handshake()

        to_send = {}
        to_drop = {}

        # Carry out the bundle assignment process to establish which bundles to send
        # over the existing contact and which to delete due to a lack of feasible routes
        # FIXME In reality this would all be getting done in parallel, on each node
        #  separately. As such, things like available storage capacity needs to be
        #  handled. E.g. what if the storage is full on node V such that node U can
        #  only transfer custody of packets to V once V has transferred some of its
        #  packets first? Perhaps we just accept what the nominal storage availability
        #  is at the start of a contact and then can only transfer a maximum volume
        #  that would reasonably fit onto the other node should it not transfer
        #  anything of its own across.
        for n in (self.u, self.v):
            m = [x for x in (self.u, self.v) if x != n][0]
            to_send[n.uid], to_drop[n.uid] = n.drm.bundle_assignment(n.vcb, m.uid)

        # Carry out the send, receive and dropping procedures
        for n in (self.u, self.v):
            m = [x for x in (self.u, self.v) if x != n][0]
            for b in to_send[n.uid]:
                # Node 'n' sends the bundle to node 'm'
                bundle = n.send_bundle(b, m.uid)
                # Node 'm' receives the bundle from node 'n'
                m.comms.receive(bundle, n.uid)
            n.drop_data(to_drop[n.uid])
            # Reset the virtual buffers and data routing module in advance of the next
            # contact opportunity
            n.vcb.reset()
            n.vb.reset()
            n.drm.reset()

    def _handshake(self):
        """
        At the start of a contact, the two nodes involved must share some initial
        information to kick off proceedings. Once the handshake is complete, each node
        can construct its virtual combined buffer, update its Contact Schedule &
        Network Resource Model, and execute the Data Routing process on the DRM. The
        handshake consists of (on each node)
        :return:
        """
        # TODO This should really be done as a send/receive process between the nodes,
        #  whereby each sends the other their CP, NRM and buffer table, and the other
        #  processes that information accordingly
        for n in (self.u, self.v):
            # identify the other node
            m = [x for x in (self.u, self.v) if x != n][0]

            # Create the virtual combined buffer by adding one's own and the other's
            # buffer contents
            [n.vcb.add_bundle(b) for b in m.buffer.bundles.values()]

            # TODO Update one's CP & NRM with the most up to date info available

    # def transmit_packets(self, u, v, to_send):
    #     """
    #     For each node, packets that have been identified as needing transferred to the
    #     current neighbour are sent from one to the other.
    #     :param u: Sending node (Node object)
    #     :param v: Receiving node (Node object)
    #     :param to_send: list of packet IDs to be transferred to the current neighbour
    #     :return:
    #     """
    #     for p in to_send:
    #
    #
    # def drop_packets(self, u, to_drop):
    #     """
    #     For each node, drop the packets that have been identified as not having a
    #     feasible route to the destination. They should be removed to free up capacity
    #     for other packets
    #     :param u: Node object with custody of the packets
    #     :param to_drop: list of packet IDS to be dropped from the buffer
    #     :return:
    #     """
