__author__ = "Christopher Lowe"


import config
from statistics import mean
import pickle


def analysis_main(nodes):
    print("*** ANALYSIS***")


    # TODO Can these be done as comprehensions so they're more compact?
    bundles_all = {}
    delivered = {}
    failed = {}
    still_onboard = {}
    journey = {}
    for node in nodes.values():
        for (bundle, time) in node.delivered:
            bundles_all[bundle.uid] = bundle

            # Add the final node and time of arrival to the journey list for the bundle
            journey[bundle.uid] = [(node.uid, time)]

            # Add the delivery info
            delivered[bundle.uid] = {
                "bundle": bundle,
                "node": node.uid,
                "time": time
            }

        for (bundle, time) in node.dropped:
            bundles_all[bundle.uid] = bundle

            # Add the final node and time of arrival to the journey list for the bundle
            journey[bundle.uid] = [(node.uid, time)]

            failed[bundle.uid] = {
                "bundle": bundle,
                "node": node.uid,
                "time": time
            }

        for bundle in node.buffer.bundles.values():
            still_onboard[bundle.uid] = node.uid
            bundles_all[bundle.uid] = bundle
            journey[bundle.uid] = [(node.uid, None)]

    # Fill in the missing information re the journey taken by each bundle
    # TODO The journey will need sorting as not necessarily going to be in order
    # TODO Add in the acquisition event to the journey
    for node in nodes.values():
        for b_uid, val in node.forwarded.items():
            for forward in val:
                if (forward['to'], forward['time']) == journey[b_uid][-1]:
                    continue
                journey[b_uid].insert(0, (
                    forward['to'],
                    forward['time']
                ))

    print(len(delivered), 'packets delivered')
    print(len(failed), 'packets failed')
    print(len(still_onboard), 'packets undelivered')
    # print(len(config.UID_JOURNEY), 'journeys were explored')
    # print(config.REQUEST_COUNT, 'data requests were made')

    latency = list()

    # For each of the packets that were delivered, get statistics around the routing
    # performance and packet journeys
    for b in delivered.values():
        # Get latency statistics
        latency.append(b["time"] - b["bundle"].birth_time)

    latency_ave = round(mean(latency), 2)
    hops_ave = round(mean([len(x) for x in journey.values()]), 2)
    del_ratio = round(len(delivered) / sum([len(delivered), len(failed)]), 2)

    print("Average latency is", latency_ave)
    print("Average hop count is", hops_ave)
    print("Delivery ratio is", del_ratio)


def analysis_overall(nodes):
    """
    Carry out top-level analysis on the whole simulation, e.g. total volume of data
    delivered, overall data latency etc, and not things that are bundle or node specific
    :param nodes:
    :return:
    """


if __name__ == "__main__":
    filename = "data"
    with open(filename, "rb") as infile:
        data = pickle.load(infile)
    analysis_main(data)