__author__ = "Christopher Lowe"


from network import Node
from misc import geometric_cdf, gast, topo_to_eci
from math import radians, pi
from random import random


class GroundBase:
    def __init__(self, lat=None, lon=None, alt=None, name=None, min_el=0):
        """
        Base class for a node that lies on the Earth's surface. This can be inherited
        into other Earth-based nodes that require a geographical location etc.

        :param lat: (float) Geodetic latitude of the target location (degrees)
        :param lon: (float) Geodetic longitude of the target location (degrees)
        :param alt: (float) altitude above sea level (m)
        :param name: (str) optional name for the target object
        :param min_el: (float) Minimum elevation above the horizon (degrees)
        """
        # super().__init__()
        self._name = name
        self._lat = lat
        self._lon = lon
        self._alt = alt
        self._min_el = min_el
        self.eci = None

    @property
    def lat(self):
        return self._lat

    @lat.setter
    def lat(self, value):
        if value > 90 or value < -90:
            print(self.name, 'latitude is out of range')
        else:
            self._lat = radians(value)

    @property
    def lon(self):
        return self._lon

    @lon.setter
    def lon(self, value):
        if value > 180 or value < -180:
            print(self.name, 'longitude is out of range')
        else:
            self._lon = radians(value)

    @property
    def alt(self):
        return self._alt

    @alt.setter
    def alt(self, value):
        if value > 8848:
            raise ValueError(
                self.name,
                'is at an altitude greater than Mt Everest, are you sure?'
            )
        else:
            self._alt = value

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, n):
        if isinstance(n, str):
            self._name = n
        else:
            raise TypeError

    @property
    def min_el(self):
        return self._min_el

    @min_el.setter
    def min_el(self, el):
        if el > 90:
            raise AttributeError(el)

        else:
            self._min_el = radians(el)

    def eci_coords(self, jd0, duration, t_step):
        """
        Return the Earth centered Inertial coordinates for the gateway for some period
        of time, at each time step
        :param jd0: initial julian date
        :param duration: length of time for which to get coordinates (s)
        :param t_step: length of time step (s)
        :return:
        """
        n_steps = int(duration / t_step)
        r_site = []
        for t in range(n_steps):
            # greenwich apparent sidereal time
            gst = gast(jd0 + (t*t_step)/(24*3600))

            # local siderial time of object
            lst = (gst + self.lon) % (2 * pi)

            r_site.append(topo_to_eci(self.lat, self.alt, lst))

        self.eci = r_site


class Gateway(GroundBase, Node):
    """
    A Gateway object is a data sink, such that it acts as a "destination"
    during the data routing process.
    """
    def __init__(
            self,
            lat=None,
            lon=None,
            alt=None,
            name=None,
            min_el=10.
    ):
        """
        Constructor
        :param lat: (float) Geodetic latitude of the target location (degrees)
        :param lon: (float) Geodetic longitude of the target location (degrees)
        :param alt: (float) altitude above sea level (m)
        :param name: (str) optional name for the target object
        :param min_el: (float) Minimum elevation above the horizon (degrees)
        """
        GroundBase.__init__(self, lat, lon, alt, name, min_el)
        Node.__init__(self, float('inf'), 0, 10, 0, 0, 1, False)
        self.task_table = None


class Target(GroundBase, Node):
    """
    Earth observation target base class, i.e. a location on the Earth's
    surface over which satellites could capture data. This base class
    implements the most simple tasking methods, where only the contact
    schedule is considered in the tasking and routing
    """
    def __init__(
            self,
            lat=None,
            lon=None,
            alt=None,
            name=None,
            min_el=70.,
            objective="capture",
            ttl=float('inf'),
            mean_time=60,
    ):
        """
        Constructor
        :param lat: (float) Geodetic latitude of the target location (degrees)
        :param lon: (float) Geodetic longitude of the target location (degrees)
        :param alt: (float) altitude above sea level (m)
        :param name: (str) optional name for the target object
        :param min_el: (float) Minimum elevation above the horizon (degrees)
        :param objective: (str) type of performance objective, options are:
            "capture" for earliest data acquisition opportunity
            "delivery" for earliest data delivery opportunity
            "latency" for minimum time between data capture and delivery
        :param ttl: (int) "Time to live" of a packet that is generated upon acquiring
            data from this target. After the TTL has expired, the packet is dropped
        :param mean_time: (int) The number of seconds, on average, between when the target
            is last acquired to when it is next added to the list of targets needing to
            be acquired. E.g. a value of 1000 means that over the long term, it is 1000
            seconds between when a target was acquired and the next time it pops up as
            needing to be acquired.
        """
        GroundBase.__init__(self, lat, lon, alt, name, min_el)
        Node.__init__(self, 0, float('inf'), 10, 0, 0, 1, True)
        self.objective = objective
        self.__ttl = ttl
        self.__is_source = True

        # Set the average time between the target being added to the list of those
        # needing to be "acquired"
        self._mean_time = mean_time

        # Times at which the target has been acquired
        self.acquisition_times = []

        # Flag indicating whether or not the Target is awaiting acquisition
        self._is_live = False

        self._assigned = False

    @property
    def is_source(self):
        return self.__is_source

    @is_source.setter
    def is_source(self, v):
        if not isinstance(v, bool):
            raise TypeError
        self.__is_source = v

    @property
    def ttl(self):
        return self.__ttl

    @ttl.setter
    def ttl(self, v):
        self.__ttl = v

    @property
    def mean_time(self):
        return self._mean_time

    @mean_time.setter
    def mean_time(self, v):
        self._mean_time = v

    @property
    def is_live(self):
        return self._is_live

    @is_live.setter
    def is_live(self, v):
        self._is_live = v

    @property
    def assigned(self):
        return self._assigned

    @assigned.setter
    def assigned(self, v):
        self._assigned = v

    def post_acquire_reset(self, time_now):
        """
        Method to reset the target following data being acquired by a satellite
        :param time_now: time at which most recent acquisition took place
        :return:
        """
        self.assigned = False
        self.acquisition_times.append(time_now)
