__author__ = "Christopher Lowe"


from misc import coe_to_mee, mee_to_cart, mee_to_coe, get_ic
import numpy as np
from math import sin, cos, sqrt, pi, radians, degrees
from scipy.integrate import odeint
from pandas import DataFrame


