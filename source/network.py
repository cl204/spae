__author__ = "Christopher Lowe"


import sys
import config
import networkx as nx
import pandas as pd
import numpy as np
from queue import PriorityQueue
from misc import assign_uid
from dataRouting import DataRoutingModule, Contact
import copy


class ContactSchedule:
    """
    Class for a Contact Schedule.
    The contact schedule defines the times, and capcities, of contact opportunities
    between nodes in the network. A dynamic graph is created, which comprises a series
    of di-graphs (one for each static graph) describing the connections between nodes.
    Di-graph n+1 illustrates the network state the moment at least one edge is lost or
    gained from the state of the graph at di-graph n
    """
    def __init__(self):
        self.digraph = nx.DiGraph()  # DiGraph from which each static graph is built
        self.dynamic_graph = []  # list containing static graph objects
        self.times = []  # list of times at which each static graph occurs


class Node:
    """
    Node class.
    A Node is an object that has the ability to store & forward and data bundles. It
    has a storage buffer and transmission characteristics
    """
    def __init__(
            self,
            capacity=float('inf'),
            transmit_rate=float('inf'),
            cost_store=0,
            cost_transmit=0,
            availability=1.,
            is_source=False
    ):
        """
        :param capacity: (int) Buffer storage capacity (# bundles)
        :param transmit_rate: (int) # bundles per second when sending
        :param priorities: (int) number of discrete data priorities able to carry
        :param cost_store: (int) cost of storing a bundle (per second)
        :param cost_transmit: (int) cost of transmitting a bundle to another node
        :param availability: (float) fraction of time (on average) available to rx/tx
        :param is_source: (bool) flag to indicate whether data is generated on board
        """
        # Assign a unique ID to the node
        self.__uid = assign_uid(config.UID_NODE)

        # instantiate the data storage buffer
        self.buffer = Buffer(
            parent=self.__uid,
            vol_total=capacity,
        )

        # Data Routing Module component
        self.drm = DataRoutingModule(self.uid)
        self.cp = None
        self.nrm = None

        # Contact Schedule
        self.cs = None

        # Virtual Buffer to hold the meta data of the bundles contained within the
        # actual buffer.
        self.vb = VirtualBuffer()

        # Virtual Combined Buffer
        # TODO do we actually need this as well as the VB??
        self.vcb = VirtualBuffer()

        # rate of data transfer
        self.tx_rate = transmit_rate

        self.comms = Comms(transmit_rate, self)

        # cost attributes, includes cost per unit time to store bundle on board node and
        # cost to transfer custody of bundle to another node
        self.cost = {
            'storage': cost_store,
            'transmit': cost_transmit
        }

        # probability of node being available to transmit or receive data
        self.availability = availability

        # flag to show if node is a data source (True). This is used in the case where
        # bundles are generated on the node at specific times
        self.source = is_source

        # A dict containing the bundles (key) and the route assigned to them (list of
        # tuples, with each tuple being the time, nodeID pair along the route)
        self.bundles_assigned = {}

        # Lists to hold details about bundles that have been processed in some way,
        # and the times at which the events happened
        self.forwarded = {}
        self.dropped = []
        self.received = {}
        self.delivered = []

    @property
    def uid(self):
        return self.__uid

    def init_drm(self, destinations, nodes):
        self.drm.setup(destinations, nodes)

    def update_cp(self, cp):
        self.cp = cp

    def update_nrm(self, nrm):
        self.nrm = nrm

    def send_bundle(self, bundle_id, to_node):
        """
        Transmission of data, shown by the bundles being removed from the buffer
        :return:
        """
        bundle = self.buffer.extract_bundle(bundle_id)
        if not self.forwarded.get(bundle_id):
            self.forwarded[bundle_id] = [{
                'to': to_node,
                'time': config.TIME
            }]
        else:
            self.forwarded[bundle_id].append({
                'to': to_node,
                'time': config.TIME
            })
        return self.comms.send(bundle)

    def drop_data(self, bundles):
        for bundle_id in bundles:
            bundle = self.buffer.extract_bundle(bundle_id)
            self.dropped.append((VirtualBundle(bundle), config.TIME))
            config.BUNDLE_DROPPED += 1
            print(
                '*** Bundle',
                bundle.uid,
                'has been dropped, total now',
                config.BUNDLE_DROPPED
            )

    def receive_data(self, bundle, from_node):
        """
        Reception of data from another node. The bundle must get added to the buffer
        and information added in relation to a new custodian.
        :param bundles:
        :return:
        """
        if self.uid in bundle.destination:
            self.delivered.append((bundle, config.TIME))

            config.BUNDLE_COUNT += 1
            print(
                'Bundle',
                bundle.uid,
                'has been delivered, total now',
                config.BUNDLE_COUNT
            )

        # if the current contact is not the destination, transfer custody of the
        # bundle into the new node's buffer
        else:
            # Log the bundle ID and time of arrival, and store the actual bundle in
            # the buffer queue ready to be forwarded later
            self.buffer.add_bundle(bundle)
            if not self.received.get(bundle.uid):
                self.received[bundle.uid] = [{
                    'from': from_node,
                    'time': config.TIME
                }]
            else:
                self.received[bundle.uid].append({
                    'from': from_node,
                    'time': config.TIME
                })


class Comms:
    """
    Communications system class, which takes care of sending/receiving data to/from
    another Node
    """
    def __init__(self, tx_rate, parent=None):
        self.tx_rate = tx_rate
        self.parent = parent

    def send(self, bundle):
        """
        Transmission of data, shown by the bundles being removed from the buffer
        :return:
        """
        return bundle

    def receive(self, bundle, frm):
        self.parent.receive_data(bundle, frm)


class Buffer:
    """
    Data buffer class
    """
    def __init__(self, parent=None, vol_total=float(sys.maxsize)):
        """
        The data buffer is located on board a specific node and contains the data
        bundles and various methods to query the state of data on board

        :param parent: (int) UID of node on which the buffer is located
        :param vol_total: (int) nominal storage volume available on the buffer
        """
        self.__node_uid = parent
        self.vol_total = vol_total
        self.bundles = dict()

    @property
    def node_uid(self):
        return self.__node_uid

    @property
    def vol_remain(self):
        return self.vol_total - self.total_bundle_vol

    @property
    def total_bundle_vol(self):
        return sum([b.size for b in self.bundles])

    @property
    def priority_classes(self):
        return len(set([b.priority for b in self.bundles]))

    @property
    def min_bundle_size(self):
        return min(
            0,
            min([b.size for b in self.bundles])
        )

    @property
    def max_expire_time(self):
        return max([b.expiry for b in self.bundles])

    @property
    def num_bundles(self):
        return len(self.bundles)

    def add_bundle(self, b):
        b.custodian = self.node_uid
        self.bundles[b.uid] = b

    def extract_bundle(self, bundle_uid):
        try:
            return self.bundles.pop(bundle_uid)
        except:
            print('')

    def is_empty(self):
        return not self.bundles


class VirtualBuffer:
    """
    The virtual buffer is an ordered version of the buffer, containing the metadata of
    each bundle required for carrying out routing, and maintaining an order such that
    bundles are efficiently extracted, highest priority first.
    """
    def __init__(self):
        self.bundles = list()

    @property
    def priorities(self):
        return set(b.priority for b in self.bundles)

    def add_bundle(self, b):
        if not self.bundles:
            self.bundles.append(VirtualBundle(b))
            return

        # if we already have at least one bundle of the same priority, find the index
        # of the first and final bundles in that priority class.
        # TODO can this be cleaner, it's a bit hacky
        if b.priority in self.priorities:
            x = [
                idx for idx, el in enumerate(self.bundles)
                if el.priority == b.priority
                and el.ttl > b.ttl
            ]
            # If there are bundles of the same priority with longer time to live,
            # insert new bundle just before them, otherwise, it goes at the end of the
            # equal-priority bundle list
            if x:
                self.bundles.insert(x[0], VirtualBundle(b))
            else:
                x = [
                    idx for idx, el in enumerate(self.bundles)
                    if el.priority == b.priority
                ]
                self.bundles.insert(x[-1]+1, VirtualBundle(b))

            return

        # If we have bundles in the list already, but none of the same priority,
        # we need to find the location at which the new bundle should be entered.
        # If we have a lower priority bundle in the list already, add into the correct
        # slot, else it must go at the top of the list
        if min(self.priorities) < b.priority:
            idx = max(
                [idx for idx, el in enumerate(self.bundles) if el.priority < b.priority]
            )
            self.bundles.insert(idx+1, VirtualBundle(b))
        else:
            self.bundles.insert(0, VirtualBundle(b))

        # TODO This is a bit hacky at the moment, because I couldn't easily deepcopy
        #  the bundles list, which is a list of PriorityQueues. As such, we're passing
        #  in the actual list of PQs from the node's buffer object, extracting the
        #  bundles one by one, highest priority first, adding a deepcopy of them to the
        #  VCB and then putting them back into the PriorityQueue so that it's unchanged
        #  going forward. This is a lot of activity for what should be a simple operation

        # temp_q = [PriorityQueue() for _ in range(max(len(bundles_u), len(bundles_v)))]
        # for bundles_n in (bundles_u, bundles_v):
        #     put_back = []
        #     for priority in range(len(bundles_n)):
        #         while not bundles_n[priority].empty():
        #             p = bundles_n[priority].get()
        #             temp_q[priority].put(copy.deepcopy(p))
        #             put_back.append(p)
        #
        #     for p in put_back:
        #         bundles_n[p[2].priority].put(p)
        #
        # for priority in range(len(temp_q)):
        #     while not temp_q[priority].empty():
        #         self.bundles.append(temp_q[priority].get()[2])

    def extract(self, n=1):
        """
        Extract the n highest priority bundles from the combined buffer
        :return:
        """
        if not self.bundles:
            return None

        extracted = []
        k = 0
        while self.bundles and k < n:
            extracted.append(self.bundles.pop(0))
            k += 1
        if n == 1:
            return extracted[0]
        return extracted

    def reset(self):
        self.bundles = list()


class Bundle:
    """
    Data bundle class
    """
    def __init__(
            self,
            destinations,
            size=1,
            priority=1,
            birth=0.,
            ttl=float('inf'),
    ):
        """
        :param destinations (set): set of node UIDs to which the bundle can be delivered
        :param size: Volume taken up in memory (and contact capacity) by the bundle
        :param priority (int): Priority level of bundle, lower number = higher priority
        :param birth: Time at which the bundle was created
        :param ttl: Total time, since birth, until bundle expires
        """
        self.__uid = assign_uid(config.UID_BUNDLE)
        self.size = size
        self.priority = priority  #
        self.birth_time = birth
        self.destination = destinations  # set of UIDs of destination nodes
        self.expiry = birth + ttl
        self.custodian = None

    @property
    def uid(self):
        return self.__uid

    @property
    def ttl(self):
        return self.expiry - config.TIME

    @property
    def age(self):
        return config.TIME - self.birth_time


class VirtualBundle:
    """Bundle object containing only the meta data required for routing rather than all
    of the payload data
    """
    def __init__(self, bundle):
        """
        :param bundle: Bundle object on which the virtual bundle is built
        """
        self.uid = bundle.uid
        self.size = bundle.size
        self.priority = bundle.priority  #
        self.destination = bundle.destination  # set of UIDs of destination nodes
        self.expiry = bundle.expiry
        self.ttl = bundle.ttl
        self.custodian = bundle.custodian
