__author__ = "Christopher Lowe"


import config
from Lib import random
import networkx as nx
import pandas as pd
from network import Node, ContactSchedule, Bundle
from dataRouting import ContactController
from misc import get_expire_time
from analysis import analysis_main
from copy import deepcopy
import cProfile
import pickle


# --- NODES ---
def init_nodes(num_nodes):
    """
    Create nodes to be used in the simulation
    :param num_nodes: (int) number of nodes
    :return: (dict) Dictionary of Node objects (KW = node ID, Value = Node object)
    """
    nodes = dict()
    for i in range(num_nodes):
        # create new node with random attributes
        node = Node(
            capacity=random.randint(10, 50),  # buffer storage capacity
            transmit_rate=10,  # data transmission rate
            cost_store=random.randint(1, 3),  # cost to store on node, per unit time
            cost_transmit=random.randint(5, 10),  # cost to transmit from node
            availability=0.9,
            is_source=True
        )
        nodes[node.uid] = node

    return nodes


# --- DATA BUNDLES ---
def add_bundles(num_bundles, nodes, start, end, ttl_min, ttl_max, dest_set,
                dest_random=False):
    """
    Add bundles to the buffer on each node so that there's something to route from the
    start of the simulation.
    *** This is not essential if bundles are being created during the simulation***
    :param num_bundles: (int) number of bundles to be added to each node
    :param nodes: (dict) Dictionary of Node objects (KW = node ID, Value = Node object)
    :param start: (int) earliest time at which a bundle could have originated
    :param end: (int) latest time at which a bundle could have originated
    :param time_now: (int) Current time
    :param dest_set: (Set) UIDs of the nodes that can be a destination for bundles
    :param dest_random: set of node IDs that are to be assigned as the bundle's destinations (if 'random', a random node
        ID is selected that does not match the bundle owner)
    """
    for n in nodes:

        # if the node is not a "source" of new bundles, then skip over it
        if not nodes[n].source:
            continue

        # For each bundle, define the size, priority, time of birth, time to live (
        # i.e. duration until it expires)
        for x in range(num_bundles):
            size = 1
            priority = random.randint(0, 3)
            birth = random.randint(start, end)
            ttl = random.randint(ttl_min, ttl_max)

            # If the destination is to be selected at random, pick from the nodes
            # that are "allowed" to be selected as destinations
            if dest_random:
                other_nodes = [x for x in dest_set if x != n]
                dests = {random.choice(other_nodes)}

            else:
                dests = dest_set

            generate_bundle(nodes[n], size, priority, birth, dests, ttl)


def generate_bundle(node, size, priority, birth, dest, ttl=float('inf')):
    """
    Generate a new bundle on board a node at a specific time

    :param node: Node object to which the bundle will be added
    :param size: Size of data bundle
    :param priority: Data bundle priority level
    :param birth: time at which the bundle was generated
    :param dest: UID of destination node
    :param time: current time
    :param ttl: time to live (until expiry of bundle)
    :param source: (Int) ID of node on which the bundle was generated

    """
    b = Bundle(
        dest,  # set containing the UIDs of nodes to which the bundle can be delivered
        size,  # volume of memory taken up by the bundle
        priority=priority,  # priority level (0, 1, 2, 3, ...)
        birth=birth,  # birth date (some time between epoch and current time)
        ttl=ttl,  # time to live (since birth) before expiration (s)
    )

    # add the bundle to the node
    node.buffer.add_bundle(b)


# --- CONTACT SCHEDULE ---
def add_random_edges(t_dyn, nodes, num_edges):
    """
    Method to initialise the edges (contacts) between nodes at different times. Each
    edge is a directed connection between a node pair, starting at a specific time and
    lasting for some duration.

    :param t_dyn: list of times at which each static graph (new edge formation) occurs
    :param nodes: dict containing each node uid (KW) and node object (value)
    :param num_edges: the maximum number of edges that can be formed at each static graph

    :return edges: (dict) Edges between nodes. KW = time at which edge is formed,
    Value = list of tuples representing each edge. An edge tuple is formed of (
    node_1_uid, node_2_uid, dict representing edge information). The dict of edge
    information contains the "capacity", "time" (formed), "duration" and "cost" (of
    transmission)
    """
    edges = {}
    nodes_used = {}

    # Add static graphs to the contact schedule
    for idx, t in enumerate(t_dyn[:-1]):
        edges[t] = []
        nodes_used[t] = []
        temp_nodes = [x for x in nodes]

        # FIXME This needs to be better and really should be a solution to handle
        #  contacts that spill over multiple graphs and contacts involving more than 2
        #  nodes.
        # prevent a node having a contact in two adjacent graphs
        if idx > 0:
            for z in nodes.keys():
                if z in nodes_used[t_dyn[idx-1]]:
                    temp_nodes.remove(z)

        # number of edges to be created in this static graph
        n = random.randint(1, num_edges)

        # Add edges to the graph
        for x in range(n):
            node1 = random.choice(temp_nodes)
            temp_nodes.remove(node1)
            nodes_used[t].append(node1)

            node2 = random.choice(temp_nodes)
            temp_nodes.remove(node2)
            nodes_used[t].append(node2)

            duration = random.randint(1, t_dyn[idx + 1] - t_dyn[idx])

            # add contact from node u --> v
            edges[t].append(
                (
                    node1,
                    node2,
                    create_edge(
                        nodes[node1].tx_rate * duration,
                        t_dyn[idx],
                        duration,
                        nodes[node1].cost['transmit']
                    )
                )
            )

            # add contact from node v --> u
            edges[t].append(
                (
                    node2,
                    node1,
                    create_edge(
                        nodes[node2].tx_rate * duration,
                        t_dyn[idx],
                        duration,
                        nodes[node2].cost['transmit']
                    )
                )
            )

    return edges


def create_edge(capacity, time, duration, cost):
    """
    Method to create an edge (contact) that represent contact between two nodes

    :param u: Sending node ID (int)
    :param v: Receiving node ID (int)
    :param capacity: Contact transfer capacity (float)
    :param time: Time at which contact starts
    :param duration: Duration of contact
    :param cost: Cost of transferring data over contact
    :return edge: Tuple containing (sending node, receiving node, dict with other data
    """
    d = {
        'capacity': capacity,  # contact capacity (data vol)
        'time': time,  # time of contact
        'duration': duration,  # duration of contact
        'cost': cost  # cost to send data from v}
    }

    return d


def init_contact_schedule(t_dyn, nodes, edges):
    """
    Create a contact schedule and populate with static graphs, nodes and edges

    A Contact schedule is a collection of static graphs that represents an Evolving
    Graph. Each static graph represents the formation of a new edge. Edge existence is
    captured by the duration of each static graph, such that a new graph
    is not required to represent loss of an edge only.

    :param t_dyn: (list) times at which static graphs are formed
    :param nodes: (dict) Dictionary of Node objects (KW = node ID, Value = Node object)
    :param edges:
    :return cs: populated ContactSchedule object
    """
    cs = ContactSchedule()

    # Add nodes to the contact schedule
    for key in nodes:
        # add node (uid) to the Contact Schedule digraph
        cs.digraph.add_node(key)

    # Add static graphs to the contact schedule by importing the base digraph from the
    # contact schedule and adding specific attributes associated with the specific time
    # step of the graph
    for idx, t in enumerate(t_dyn[:-1]):
        g = nx.DiGraph(cs.digraph)
        g.graph['time'] = t
        g.add_edges_from(edges[t])
        cs.dynamic_graph.append(g)

    cs.times = t_dyn

    return cs


def init_timings(start, end, n_dyn):
    """
    Define the times at which static graphs occur (i.e. times at which some change in
    graph topology occurred)

    :param start: (int) Initial time
    :param end:  (int) Final time
    :param n_dyn: (int) Number of static graphs
    :return t_dyn: (list) Times at which a new static graph forms
    """
    # set to hold the times at which the static graphs occur
    t_dyn = set()

    # while there are fewer times than the number of static graphs,
    # add a random time at which the next graph will occur
    while len(t_dyn) < n_dyn:
        n = random.randint(start, end)
        t_dyn.add(n)

    t_dyn = list(t_dyn)
    t_dyn.sort()

    return t_dyn


def get_evolving_graph(cs, ttl):
    """
    Based on the current contact schedule, create a copy that can be used for routing
    purposes. Only need to use the static graphs that occur before expiry of the last
    piece of data on board

    :param cs: Contact Schedule object
    :param ttl: Time at which expiry happens
    :return:
    """
    # If the bundle expiry date is earlier than the time of the final static graph
    if ttl < cs.times[-1]:
        # Identify the index of the static graph immediately following expiry
        # (don't need to look beyond this)
        idx = next(i for i, value in enumerate(cs.times) if value > ttl)

    else:
        # set the index to call the final static graph
        idx = len(cs.times)-1

    cs_new = ContactSchedule()
    cs_new.digraph = cs.digraph.copy()

    for k in range(idx):
        cs_new.dynamic_graph.append(cs.dynamic_graph[k].to_directed())
        cs_new.times.append(cs.times[k])

    return cs_new


def build_contact_plan(cs, nodes):
    """
    Return a table (pandas Dataframe) of contact opportunity
    :param cs:
    :param nodes:
    :return:
    """
    k = 0
    contacts = []
    for dg in cs.dynamic_graph:
        for edge in dg.edges:
            edge_attribs = dg.adj[edge[0]][edge[1]]
            contacts.append(
                {
                    "id": k,
                    'sending': edge[0],
                    'receiving': edge[1],
                    'start': edge_attribs['time'],
                    'end': edge_attribs['time'] + edge_attribs['duration'],
                    'rate': nodes[edge[0]].comms.tx_rate
                }
            )
            k += 1

    contact_table = pd.DataFrame(contacts)
    return contact_table


def build_network_resource_model(nodes):
    """
    Create the network resource model.
    This is a simple table with a row for each network node and a column for each
    resource. Each entry is the nominal value for that resource.
    :return:
    """
    resources = []
    for node_id, node in nodes.items():
        resources.append(
            {
                "id": node_id,
                'dl_avail': 1.,
                'isl_avail': node.availability,
                'storage': node.buffer.vol_total,
                'energy': float('inf')
            }
        )
    nrm = pd.DataFrame(resources)
    return nrm


def routing_sim(cs, nodes, destinations, p_gen_prob, ttl_min, ttl_max,
                dest_random=False):
    """
    Carry out data routing at each static graph in the Contact Schedule

    :param cs:
    :param nodes:
    :param destinations: Set of node UIDs to be considered "destinations"
    :param p_gen_prob: float [0, 1] giving the probability of a new bundle being
        generated on each node at each graph
    :param dest_random: boolean showing whether (True) or not (False) the destination
        for a new bundle should be picked at random from the list of destinations
    :return:
    """
    previous_time = cs.times[0]

    # for each static graph, i.e. each time a new contact is formed between a node pair
    for x in range(len(cs.dynamic_graph) - 1):
        g = cs.dynamic_graph[0]

        # TODO Perhaps want to have a better "global" setting for this
        config.TIME = g.graph['time']

        print(cs.times[0])
        u_v_sets = list()

        # add new bundles to the network
        add_bundles(
            1,
            nodes,
            previous_time,
            cs.times[0],
            ttl_min,
            ttl_max,
            destinations,
            dest_random
        )

        # for each node pair u, v with a connection
        for u, v, capacity_uv in g.edges(data='capacity'):

            # if we've not already evaluated this node pair
            if {u, v} not in u_v_sets:
                # Route Discovery on each node prior to the contact occurring. In real
                # life, this will be invoked by the platform at some time between
                # contacts that is convenient (e.g. between data acquisition events)
                for k in (u, v):
                    other = [x for x in (u, v) if x != k][0]
                    nodes[k].drm.route_discovery(nodes[k].cp, nodes[k].nrm, other)

                    # Add the bundle information to the virtual buffer and virtual
                    # combined buffer, in order of 1. Priority and 2. remaining TTL
                    [nodes[k].vb.add_bundle(b) for b in nodes[k].buffer.bundles.values()]
                    [nodes[k].vcb.add_bundle(b) for b in nodes[k].buffer.bundles.values()]

                # Carry out the contact between nodes u & v, including allocation of
                # bundles to optimal routes, transfer of bundle custody and dropping of
                # bundles that will expire before delivery
                contact = ContactController(nodes[u], nodes[v])
                contact.execute_contact()

            u_v_sets.append({u, v})

        # Remove the static graph and time parameter related to the one that has just
        # been evaluated
        cs.dynamic_graph.pop(0)
        previous_time = cs.times[0]
        cs.times.pop(0)


# --- MAIN RUN METHOD ---
def main():
    # --- INPUTS ---
    epoch = 0  # start time
    end = 1000  # end time
    time = 10  # current time
    config.TIME = time
    n_dyn = int((end-time)/10)  # number of static graphs in the dynamic graph
    num_nodes = 10  # number of nodes in the analysis
    num_edges = 1  # maximum number of edges per graph
    num_bundles = 10  # number of bundles on each node (originally)
    p_gen_prob = 0.1  # probability of a new bundle being generated at each static graph

    # --- CREATE ELEMENTS ---
    nodes = init_nodes(num_nodes)
    node_uids = {n for n in nodes.keys()}

    # add some bundles to the nodes to get us started off
    add_bundles(num_bundles, nodes, epoch, time, 200, 500, node_uids,
                dest_random=True)

    # get times at which the static graphs occur
    t_dyn = init_timings(time, end, n_dyn)

    # create the edges that are to exist on each static graph
    edges = add_random_edges(t_dyn, nodes, num_edges)

    # create the contact plan and network resource model
    cs = init_contact_schedule(t_dyn, nodes, edges)
    cp = build_contact_plan(cs, nodes)
    nrm = build_network_resource_model(nodes)

    # Initiate the Data Routing Module, Contact Plan and Network Resource Model
    for n in nodes.values():
        n.init_drm(node_uids, node_uids)
        n.update_cp(cp)
        n.update_nrm(nrm)

    # --- ROUTE BUNDLES ---
    routing_sim(cs, nodes, node_uids, p_gen_prob, 200, 500, True)

    # --- SAVE DATA SO IT CAN BE ANALYSED SEPARATELY ---
    save_data(nodes)

    # --- ANALYSIS ---
    analysis_main(nodes)


def save_data(*args):
    filename = 'data'
    with open(filename, 'wb') as outfile:
        pickle.dump(args[0], outfile)


if __name__ == "__main__":
    # main()
    cProfile.run('main()')
    # pass
