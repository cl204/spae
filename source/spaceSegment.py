__author__ = "Christopher Lowe"


import numpy as np
from network import Node
from misc import gsite, random_topology, mee_to_cart, mee_to_coe, coe_to_mee, \
    slant_range
from main import init_contact_schedule, create_edge, add_bundles, routing_sim
from analysis import analysis_main
from groundSegment import Gateway, Target
from math import sin, cos, sqrt, radians, pi, asin
from scipy.integrate import odeint
import cProfile


class Mission:
    """
    The Constellation class is a representation of multiple spacecraft
    """
    def __init__(self, jd0, duration, step_size):
        self.jd_start = jd0
        self.duration = duration
        self.step = step_size

        self._satellites = {}
        self._gateways = {}
        self._targets = {}
        self.edges = None
        self.cs = None
        self.isl_dist = None

    def edge_schedule(self, times, isl_dist=0):
        """
        Method to construct the "connection matrix" for connections between
        targets, satellites and gateways.

        Satellites are considered to be "in contact" with targets and
        gateways when they are above the minimum elevation above the
        horizon, and in contact with other satellites when closer than some
        threshold distance from another node with whom they are able to
        communicate.

        :param times: (list) array of the times for the mission simulation
        :param isl_dist: (int/float) distance (in m) from the

        :return edges: (list) edges that exist at each time step. Each item
            contains the time and tuples of node-pairs between which a
            connection is possible
        """
        self.isl_dist = isl_dist
        edges = {t: [] for t in times}
        positions = {x: self.satellites[x].orbit.eci for x in self.satellites}

        for u in self.satellites:
            v_all = [v for v in self.satellites if v is not u]
            for v in v_all:

                pos_vec = np.subtract(positions[u][:, 0:3], positions[v][:, 0:3])
                sep = np.linalg.norm(pos_vec, axis=1, keepdims=True)
                vis = sep < self.isl_dist
                [edges[t].append((u, v)) for idx, t in enumerate(times) if vis[idx]]

            for g in self.gateways:
                max_range = slant_range(
                    self.satellites[u].orbit.coe0[0],  # TODO make coe0 a dict
                    radians(self.gateways[g].min_el)
                )
                pos_vec = np.subtract(
                    positions[u][:, 0:3],
                    np.array(self.gateways[g].eci)
                )
                sep = np.linalg.norm(pos_vec, axis=1, keepdims=True)
                vis = sep < max_range
                [edges[t].append((u, g)) for idx, t in enumerate(times) if vis[idx]]

        self.edges = edges

    def build_contact_schedule(self, nodes):
        """
        Construct a dict containing information about each contact between
        nodes in the network, structured in such a way that can be used to
        build a Spae-compatible evolving graph. Each entry in the dict is
        a time (key) at which a new contact begins, and the value is the
        contact attributes, such as the nodes involved, the duration and the
        total data transfer capacity
        :param nodes:
        :return:
        """

        schedule = [(k, self.edges[k]) for k in self.edges]
        edges = {k: [] for k in self.edges.keys()}
        idx = -1

        for es in schedule:
            idx += 1

            # if an edge exists (i.e. not just the time)
            while es[1]:
                e = es[1][0]
                u = e[0]
                v = e[1]

                # find the time at which this edge ceases to exists
                counter = 0
                schedule[idx][1].remove(e)

                # for each edge downstream of the contact we're looking at
                for downstream in schedule[idx + 1:]:
                    if e in downstream[1]:
                        counter += 1
                        schedule[idx + counter][1].remove(e)

                    else:
                        t_span = downstream[0] - es[0]

                        # if the "other" node in the contact is a gateway
                        if isinstance(nodes[v], Gateway):
                            # add an edge from the satellite to the Gateway (download)
                            edges[es[0]].append((
                                u,
                                v,
                                create_edge(
                                    self.satellites[u].tx_rate * t_span,
                                    es[0],
                                    t_span,
                                    self.satellites[u].cost['transmit']
                                ))
                            )
                            # add an edge with zero capacity in the other direction
                            edges[es[0]].append((
                                v,
                                u,
                                create_edge(
                                    0,
                                    es[0],
                                    t_span,
                                    0.
                                ))
                            )

                        # if the "other" node in the contact is a target
                        elif isinstance(nodes[v], Target):
                            # add a zero capacity edge from the satellite
                            edges[es[0]].append((
                                u,
                                v,
                                create_edge(
                                    0,
                                    es[0],
                                    t_span,
                                    0.
                                ))
                            )
                            # add an edge with inf capacity in the other direction
                            edges[es[0]].append((
                                v,
                                u,
                                create_edge(
                                    float('inf'),
                                    es[0],
                                    t_span,
                                    0.
                                ))
                            )

                        # if both nodes in the contact are satellites
                        else:
                            # add an edge to the contact schedule
                            edges[es[0]].append((
                                u,
                                v,
                                create_edge(
                                    self.satellites[u].tx_rate * t_span,
                                    es[0],
                                    t_span,
                                    self.satellites[u].cost['transmit']
                                ))
                            )

                        break

        cs = {e[0][2]['time']: e for e in edges.values() if e}

        # Add an inf capacity contact between each gateway and the "base" gateway,
        # at each contact event. This way we can just have the base gateway as the
        # destination for packets and the source for tasking commands. This basically
        # represents the fact that all of the gateways are linked through the internet
        # and by connecting to one you effectively connect to them all
        # TODO Using the "name" attribute is not a robust way to find the "Base"
        #  gateway, really we should have an attribute that identifies whether or
        #  not the gateway is the base station, but would then need to ensure only
        #  one can have this attr as True
        base_gate_uid = [
            g.uid for g in self.gateways.values() if g.name == "Base Gateway"
        ][0]
        for e, v in cs.items():
            for g_uid, g in self.gateways.items():
                if g_uid != base_gate_uid:
                    v.append((
                        base_gate_uid,
                        g_uid,
                        {
                            'capacity': float('inf'),
                            'time': e,
                            'duration': 10,
                            'cost': 0
                        }
                    ))
                    v.append((
                        g_uid,
                        base_gate_uid,
                        {
                            'capacity': float('inf'),
                            'time': e,
                            'duration': 10,
                            'cost': 0
                        }
                    ))

        self.cs = cs

    @property
    def targets(self):
        return self._targets

    @targets.setter
    def targets(self, v):
        self._targets = self.add_nodes(v)

    @property
    def satellites(self):
        return self._satellites

    @satellites.setter
    def satellites(self, v):
        self._satellites = self.add_nodes(v)

    @property
    def gateways(self):
        return self._gateways

    @gateways.setter
    def gateways(self, v):
        self._gateways = self.add_nodes(v)

    @staticmethod
    def add_nodes(node_dict):
        if isinstance(node_dict, (list, set, tuple)):
            return {n.uid: n for n in node_dict}

        elif isinstance(node_dict, dict):
            return node_dict

        else:
            raise TypeError(
                node_dict,
                "is not of a valid type, must be a dict, list, tuple or set"
            )


class Spacecraft(Node):
    def __init__(
            self,
            capacity,
            tx_rate,
            priorities=1,
            cost_store=0,
            cost_transmit=0,
            avail=1.,
            is_source=True
    ):
        super().__init__(
            capacity,
            tx_rate,
            priorities,
            cost_store,
            cost_transmit,
            avail,
            is_source
        )

        self.orbit = None
        self.tasks = {}
        self.task_table = None

    def get_orbit(self, initial, ele_type, jd0, duration, step):
        self.orbit = Orbit(jd0, initial, ele_type)
        self.orbit.propagate_orbit(duration, step)

    def update_task_table(self, table):
        pass


class Orbit:
    mu = 3.986005e+14  # Earth gravitational constant
    j2 = 0.00108263  # J2 Earth oblateness
    re = 6371000.  # Earth radius - average (m)

    def __init__(self,
                 jd0,
                 initial_state,
                 ele_type='coe'
                 ):

        # Get the initial conditions in Classical Orbit Elements
        self.coe0 = None
        self.mee0 = None
        self.get_ic(initial_state, ele_type)
        self.jd0 = jd0

        self.t = None
        self.mee = None  # initialise mod eq elements at each time step
        self.coe = None  # init classical orbit elements
        self.eci = None  # init cartesian elements (ECI frame)
        self.propagated = False

    def get_ic(self, init_conditions, ele_type):
        """
        Get modified equinoctial element form of the orbit parameters
        :param init_conditions: Initial conditions (list of 6 variables)
        :param ele_type: String defining the "type" of element. "mee", "coe", "eci"
        :return:
        """
        # if already using modified equinoctial elements, just return them
        if ele_type == 'mee':
            self.mee0 = init_conditions
            self.coe0 = mee_to_coe(init_conditions)

        # If using classical elements, convert and return
        elif ele_type == 'coe':
            self.mee0 = coe_to_mee(init_conditions)
            self.coe0 = init_conditions

        # TODO add in conversions to MEE in case incoming elements cartesian, or other
        else:
            raise AttributeError(
                f'{ele_type} is not supported as a type of orbit element, choose "mee" or "coe"')

    def propagate_orbit(self, duration, step):

        eq = self.eq_of_mo_mee  # bind to equations of motion method
        t = np.arange(0, duration, step)  # time array used for simulation

        # Carry out numerical integration, returning a numpy array of the
        # modified equinoctial elements
        mee = odeint(eq, self.mee0, t)

        # get keplerian and cartesian results from modified equinoctial
        # elements
        eci = [tuple(mee_to_cart(x)) for x in mee]
        coe = [tuple(mee_to_coe(x)) for x in mee]

        self.t = t
        self.mee = mee
        self.coe = np.array(coe)
        self.eci = np.array(eci)
        self.propagated = True

    def eq_of_mo_mee(self, u, t):
        """
        Orbit equations of motion for Modified equinoctial elements
        :param t:
        :param u:
        :param mu:
        :return:
        """

        # initialise variables to input
        p = u[0]
        f = u[1]
        g = u[2]
        h = u[3]
        k = u[4]
        L = u[5]

        # calculate support parameters
        sinL = sin(L)
        cosL = cos(L)
        w = 1 + f * cosL + g * sinL
        x = sqrt(p / self.mu)
        r = p / w
        s2 = 1 + h ** 2 + k ** 2

        Dr, Dt, Dn = self.pert_j2_mee(h, k, L, r)

        # calculate rates of change in variables
        dp = ((2 * p * x) / w) * Dt
        df = x * (sinL * Dr + (((w + 1) * cosL + f) / w) * Dt - (((h * sinL - k * cosL) * g) / w) * Dn)
        dg = x * (-cosL * Dr + (((w + 1) * sinL + g) / w) * Dt + (((h * sinL - k * cosL) * f) / w) * Dn)
        dh = (x * s2 * cosL / (2 * w)) * Dn
        dk = (x * s2 * sinL / (2 * w)) * Dn
        dL = (sqrt(self.mu * p) * (w / p) ** 2) + (1 / w) * x * (h * sinL - k * cosL) * Dn

        # return rates of change variables
        return [dp, df, dg, dh, dk, dL]

    def pert_j2_mee(self, h, k, l, r):
        """ calculate perturbation forces due to J2 in MEq frame

        :param h:
        :param k:
        :param l:
        :param r:
        :return:
        """

        x = self.mu * self.j2 * self.re ** 2 / r ** 4
        y = (1 + h ** 2 + k ** 2) ** 2
        sl = sin(l)
        cl = cos(l)

        dr = -(3 / 2) * x * (1 - (12 * (h * sl - k * cl) ** 2) / y)
        dt = -12 * x * (((h * sl - k * cl) * (h * cl + k * sl)) / y)
        dn = -6 * x * (((1 - h ** 2 - k ** 2) * (h * sl - k * cl)) / y)

        return dr, dt, dn


def setup_satellites(jd_start, duration, t_step):
    # Prepare the satellite objects and put them into a dict
    num_sats = 10
    sma_range = (7000000, 7000000)
    inc_range = (radians(89), radians(90))

    # Generate a random satellite network using the orbit ranges provided
    sma, inc, raan, ta = random_topology(num_sats, sma_range, inc_range)

    # Create all of the satellite objects and propagate their orbits
    satellites = {}
    for x in range(num_sats):
        # Instantiate the satellite class, define the initial orbit conditions,
        # propagate the orbit and add the satellite to the collection
        s = Spacecraft(
            1000,  # buffer capacity (# of packets)
            10,  # transmission rate (packets per second)
            3,  # number of priority levels available in buffer
            1,  # cost per unit time to store a packet on board
            0,  # cost to transfer a packet to another node
            1.,  # probability of satellite availability
            True  # flag to indicate whether packets can originate on the satellite
        )
        ic = [sma[x], 0., inc[x], raan[x], 0., ta[x]]
        s.get_orbit(ic, 'coe', jd_start, duration, t_step)
        satellites[s.uid] = s

    print("Orbit propagation complete")
    return satellites


def setup_gateways(jd_start, duration, t_step):
    gateways = list()

    # Create a "Base" Gateway that will always connect to each of the other Gateway
    # objects but never connect to the satellites. This is then used as a central node
    # to which all data is to be delivered and from which all commands are generated.
    gateways.append(Gateway(0, 0, 0, "Base Gateway", 100))

    # Create the individual Gateway nodes to which satellites are intermittently connected
    gateways.append(Gateway(90, 0, 0, 'North Pole gateway'))
    gateways.append(Gateway(-90, 0, 0, 'South Pole gateway'))

    for g in gateways:
        g.eci_coords(jd_start, duration, t_step)

    return {g.uid: g for g in gateways}


def main():
    # The general flow of this test script is:
    # - Define satellites
    # - Define gateways
    # - Define targets (if applicable, it may be the case that the
    #    satellite acquire data ad-hoc rather than in relation to specific
    #    targets)
    # - Propagate the orbits to obtain Earth-centred Earth-fixed positions
    #    over time
    # - Evaluate positions of satellites, gateways and targets and extract
    #    contact opportunities (i.e. times at which the nodes are able to
    #    "communicate" or "acquire"
    # - Evaluate operations through simulated data flow. This could include
    #     both scheduling of data acquisition events (if explicit targets
    #     exist) and routing of data through the network based on target and
    #     data packet objectives
    # - Analyse results of the simulation

    # Orbit propagation time info
    jd_epoch = 0.  # time from which packets already on board can originate
    jd_start = 0.05  # start date (julian date)
    duration = int(1. * 86400)  # duration of simulation (s)
    t_step = 10  # step size (s)

    satellites = setup_satellites(jd_start, duration, t_step)
    gateways = setup_gateways(jd_start, duration, t_step)

    # Instantiate the (empty) constellation class
    mission = Mission(jd_start, duration, t_step)
    mission.satellites = satellites
    mission.gateways = gateways

    all_times = [i for i in range(0, duration, t_step)]
    all_nodes = {**satellites, **gateways}

    # max distance (m) at which point communication can be maintained between satellites
    max_contact_dist = 1000000
    # Get the contact schedule for satellites in the constellation
    mission.edge_schedule(all_times, max_contact_dist)
    print("Contact edges found")

    mission.build_contact_schedule(all_nodes)
    print("Contact opportunities formatted")

    # add a set of packets to each of the satellite buffers
    add_bundles(
        10,
        mission.satellites,
        jd_epoch * 86400,
        all_times[0],
        all_times[0],
        60 * 60,
        120 * 60,
        set([x for x in gateways if gateways[x].name != "Base Gateway"])
    )

    # create the contact schedule
    cs = init_contact_schedule(
        [x for x in mission.cs.keys()],
        all_nodes,
        mission.cs
    )
    print("Contact Schedule built")

    del_all, fail_all = routing_sim(
        cs,
        all_nodes,
        [g for g in gateways if gateways[g].name != "Base Gateway"],
        0.1,
        60 * 60,
        120 * 60,
    )

    analysis_main(del_all, fail_all)
    pass


if __name__ == "__main__":
    cProfile.run('main()')
    # main()
