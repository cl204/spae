"""Payload tasking of satellites for a number of ground-based targets

Tasking here is referred to as the act of assigning data acquisition
"tasks" to satellites based on the performance metric associated with the
target (e.g. earliest capture or earliest delivery), the predicted contact
schedule of the satellites and gateways, and other attributes related to the
network (e.g. cost of acquisition if a federated system).
"""


__author__ = "Christopher Lowe"


import config
from misc import random_topology, get_expire_time, add_to_queue, assign_uid, geometric_cdf
from spaceSegment import Spacecraft, Mission
from groundSegment import Gateway, Target
from main import init_contact_schedule, get_evolving_graph, generate_bundle
from network import Bundle, ContactSchedule
import cProfile
from dataRouting import Spae
from math import radians
from random import random, randint
from analysis import analysis_main
from queue import PriorityQueue
import copy


class Task:
    def __init__(self, targets, satellites, gateways, dyn_graph):
        """
        Scheduling class that assigns "acquisition tasks" to satellites
        based on the targets under observation and the contact schedule. The
        connection opportunities between targets, satellites and gateways,
        and the network state in terms of congestion, acquisition/relay cost
        and data priority should all be considered.
        """
        self.targets = targets
        self.satellites = satellites
        self.gateways = gateways
        self.dyn_graph = dyn_graph
        self.sats_acquired = set()

    def assign(self, target, request):
        """
        Method to identify the node that should be tasked with data capture
        of the target. The method used to identify the acquiring satellite is dictated
        by the property of the Target being acquired, i.e. whether it should follow a
        "earliest capture", "earliest delivery" or "minimum latency" approach. A
        shortest-path-based method is then used to identify the node that will best
        suit the target property. This node must be able to receive the tasking
        information prior to its intended capture event, in order be considered
        feasible. This could be directly from the gateways or via ISL.

        Once this is done, a Tasking table is distributed throughout the network
        containing information about which nodes should acquire data over which
        targets, at which times.

        :param target: (Target) Target object representing the target for which an
            acquiring node is being found.
        """
        sat_time_acquire = None
        # Execute the relevant tasking method based on the acquisition/delivery
        # objective of the target object being considered.
        if request.objective == "capture":
            sat_time_acquire = self.task_capture(target)

        elif request.objective == "deliver":
            sat_time_acquire = self.task_deliver(target)

        elif request.objective == "latency":
            sat_time_acquire = self.task_latency(target)

        else:
            AttributeError(target.objective)
            print(
                target.objective,
                "not a valid target performance objective string"
            )

        return sat_time_acquire

    def task_capture(self, target):
        """
        Find the UID of the satellite that first makes contact with the target and
        assign this satellite to target acquisition at that contact time
        :param graph:
        :param target:
        """
        # TODO (CL) Should consider buffer storage capacity in this

        # TODO (CL) Should consider downstream resources to ensure delivery before
        #  expiry is possible

        t_uid = target.uid
        for g in self.dyn_graph.dynamic_graph:
            if g.out_degree[t_uid]:
                # TODO (CL) if two satellites happen to make contact with the target at
                #  exactly the same time, we're currently not accounting for this
                node, _ = [x for x in g.adj[t_uid].items()][0]
                return node, g.graph['time']

        print("Target", target.uid, "could not be assigned")

    def task_deliver(self, target):
        """
        Assign data capture task to the satellite that has shortest possible combined
        route from the gateway to the target and the acquiring satellite to the gateway.

        This is achieved by first identifying the first opportunity for a satellite to
        acquire data and then finding the shortest path to the gateway for delivery of
        the packet. Continue this process for each other satellite until all of the
        combined journeys are longer than the optimal one found so far.
        :param graph:
        :return:
        """
        t_uid = target.uid
        opt_jny = None
        opt_jny_end = float('inf')
        base_gateway = [
            g.uid for g in self.gateways.values() if g.name == "Base Gateway"
        ][0]

        # While there are satellites that have not been considered as the "acquiring" node
        while len(self.satellites) > len(self.sats_acquired):
            jny_task = self.tasking_route(
                self.dyn_graph,
                base_gateway,
                t_uid,
                opt_jny_end
            )

            # If no feasible journeys are found to satellites to acquire data before
            # the current best combined journey is complete, we can exit the process as
            # we've found the best
            if not jny_task:
                break

            sat_acquire = jny_task[-2][0]
            time_acquire = jny_task[-1][1]
            self.sats_acquired.add(sat_acquire)

            opt_jny_new = self.delivery_route(
                time_acquire,
                sat_acquire,
                t_uid,
                self.dyn_graph,
                opt_jny_end
            )
            if opt_jny_new:
                opt_jny = opt_jny_new
                opt_jny_end = opt_jny[-1][1]
        if opt_jny:
            return opt_jny[0][0], opt_jny[0][1]
        else:
            print("Could not find a feasible journey for target", t_uid)
            return False

    def task_latency(self, target):
        pass

    def tasking_route(self, ev_graph, s, d, shortest_jny):
        """
        Simplified version of Dijkstra's algorithm for evolving (time-varying) graphs,
        in which only contact opportunities are considered (i.e. no
        resource-consideration). Because of this, we are not routing a packet as such,
        we are simply finding the shortest path between the source and destination.

        In the tasking context, this isn't quite Dijkstra in the sense that we are not
        just interested in the shortest path from source to destination,
        we are interested in the shortest paths from source to dest, which have
        different satellites as the penultimate (data acquisition) nodes. As such,
        once the route for one satellite has been found, this satellite cannot again be
        the acquiring node.

        :param ev_graph: Evolving Graph
        :param s: source node ID
        :param d: destination node ID
        :param shortest_jny: Length of the optimum journey. This is used as a max
        limit that, if exceeded by this tasking phase, renders the process void. E.g.
        if we have already discovered a task-acquire-deliver process that takes 30 mins,
        and the next shortest task route is >30 mins, we won't find anything better
        than what we already have
        :return jny:
        """
        jny = []
        journeys_incomplete = PriorityQueue()
        journeys_complete = PriorityQueue()

        # Start a new search from the current custodian by creating a journey from
        # there, at the current time. Journey format is a list containing tuples of
        # custodian-time pairs.
        # We want to look downstream and create a new journey for every time we make
        # contact with a node, so long as:
        # --> the node has not been visited already on the journey
        # --> the total journey time does not exceed the optimal combined
        #     (tasking + delivery) time.
        jny.append((s, ev_graph.dynamic_graph[0].graph["time"]))

        # Automatically add a contact to the other gateways to avoid searching from the
        # base to each of the other gateways
        for x in self.gateways:
            if x == s:
                continue
            jny_new = copy.deepcopy(jny)
            jny_new.append((x, ev_graph.dynamic_graph[0].graph["time"]))
            journeys_incomplete.put((jny_new[-1][-1], jny_new))

        jny = journeys_incomplete.get()[1]

        # While the (incomplete) journey that is currently being explored (which is the
        # shortest one available) is shorter than the shortest optimal journey,
        # then there's still potentially something to explore
        while jny[-1][-1] < shortest_jny:
            # Set of nodes that have already been visited along this journey
            visited_set = set([x[0] for x in jny])
            t_prev_contact = jny[-1][-1]
            idx = next(i for i, val in enumerate(ev_graph.times) if val > t_prev_contact)

            if shortest_jny < ev_graph.times[-1]:
                idx_end = next(
                    i for i, val in enumerate(ev_graph.times)
                    if val > shortest_jny
                )
            else:
                idx_end = len(ev_graph.times) - 1

            # for each static graph downstream of the most recent hop in the journey
            # and up until the time at which our best combined journey occurs (don't
            # need to look beyond this)
            for dg in ev_graph.dynamic_graph[idx:idx_end]:
                # if the current node has no out-going edges, go to the next static graph
                if dg.out_degree[jny[-1][0]] == 0:
                    continue
                # identify the current contact information (nodes and times etc)
                neighbours = [x[0] for x in [x for x in dg.adj[jny[-1][0]].items()]]
                for neighbour in neighbours:
                    # if the neighbour has already been visited from the current node,
                    # progress to the next iteration
                    if neighbour in visited_set:
                        continue

                    # We know it's feasible, so create a new journey that is based on the
                    # journey up to this point
                    jny_new = copy.deepcopy(jny)

                    # add the current contact information to the current journey
                    jny_new.append((neighbour, dg.graph["time"]))

                    # Add the receiving node of the current contact to the set of
                    # neighbours having already been visited along this journey
                    visited_set.add(neighbour)

                    # Add the journey to its appropriate queue
                    # If the current contact is not one of the feasible destinations for
                    # the packet, it goes in the "incomplete" queue, else it goes in the
                    # complete queue
                    if neighbour != d:
                        journeys_incomplete.put((jny_new[-1][-1], jny_new))
                    else:
                        # If the satellite acquiring the data is one that has already
                        # been evaluated as an acquiring sat, skip
                        if jny_new[-2][0] in self.sats_acquired:
                            continue
                        journeys_complete.put((jny_new[-1][-1], jny_new))
                        shortest_jny = min(jny_new[-1][-1], shortest_jny)

            # if there is at least one incomplete journey in the PQ, get the lowest
            # cost journey from the incomplete list. Otherwise exit the process as
            # we've searched all of the possible options
            if journeys_incomplete.queue:
                jny = journeys_incomplete.get()[1]
            else:
                break

        # If there's at least one complete journey found, return the highest value one
        if not journeys_complete.empty():
            return journeys_complete.get()[1]

    def delivery_route(self, time_acquire, sat_acquire, t_uid, graph, opt_jny_end):
        """
        Find the shortest path between the acquiring satellite and the gateway. To do
        this, create a theoretical packet that starts at the contact between the sat
        and the target, and route this to the packet's destinations. If delivery cannot
        be achieved before the best route we've found already, then we exit the process
        :param time_acquire: Time at which the packet was acquired
        :param sat_acquire: Satellite UID that acquired the packet
        :param t_uid: UID of the Target
        :param graph: Evolving Graph
        :param opt_jny_end: Delivery time of the best route found so far
        :return opt_jny: Optimal route taken by the packet, if found. This is a list of
            tuples, each containing a (node_uid, time) pair
        """
        opt_jny = None
        fake_packet = Bundle(
            set([x for x in self.gateways]),
            birth=time_acquire,
            time=time_acquire,
            source_node=t_uid
        )
        fake_packet.custodians.append((sat_acquire, time_acquire))

        # Carry out routing for a theoretical packet, to find the highest value
        # journey fom the target to a gateway, starting at the current time
        all_nodes = {**self.satellites, **self.gateways}
        idx = next(
            i for i, val in enumerate(graph.times) if val > time_acquire - 1
        )

        # Identify the static graph at which the current custodian is next in
        # contact with another node.
        k = next(
            x for x, val in enumerate(graph.dynamic_graph[idx + 1:])
            if len(val.adj[sat_acquire]) > 0
        )

        cs = ContactSchedule()
        cs.digraph = graph.digraph
        cs.dynamic_graph = graph.dynamic_graph[idx+k+1:]
        cs.times = graph.times[idx+k+1:]

        # TODO the Spae constructor is overkill for our needs, since we're only
        #  routing a single packet at a time. Also, we're not at a contact as such,
        #  although I suppose we could just make the assumption that we've
        #  progressed to the next available contact and go from there?
        spae = Spae(all_nodes, sat_acquire, sat_acquire, cs)
        spae.route_packet(fake_packet)

        # If a feasible route is found to deliver the packet, check to see if this
        # is sooner than the current best delivery journey. If it is, set this as
        # the optimal journey
        if spae.routed_packets[0]["jny"].times[-1] < opt_jny_end:
            opt_jny = [
                x for x in zip(
                    spae.routed_packets[0]["jny"].nodes,
                    spae.routed_packets[0]["jny"].times
                )
            ]

        return opt_jny


class Request:
    def __init__(
            self,
            target,
            obj,
            t,
            priority=0,
            acquire=float('inf'),
            deliver=float('inf')
    ):
        """
        A request is made in order to prompt tasking of a satellite to capture data
        over a target in such a manner to meet the demands of request. E.g. the type of
        objective defines which tasking method is used, and the acquisition and
        delivery deadlines provide inputs to which acquisition/delivery events are
        "feasible".

        :param target: (int) UID of target object to be acquired
        :param obj: (str) type of performance objective, options are:
            "capture" for earliest data acquisition opportunity
            "delivery" for earliest data delivery opportunity
            "latency" for minimum time between data capture and delivery
        :param t: (int) time at which the request was made
        :param priority: (int) Priority level of the request. 0 is low priority,
            higher priority requests are serviced first, but come at greater cost
        :param acquire: (int) time by which the acquisition must occur
        :param deliver: (int) time by which the delivery of the data must occur
        """
        self.__uid = assign_uid(config.UID_REQUEST)
        self.target = target
        self.objective = obj
        self.t_requested = t
        self.priority = priority
        self.acquire_deadline = acquire
        self.deliver_deadline = deliver

    @property
    def uid(self):
        return self.__uid


class TaskTable:
    """
    The AssignationTable stores the acquisition events that have
    been allocated by the Task scheduler. Each event outlines the target and time of
    acquisition, for each satellite in the network
    """
    def __init__(self, satellites, targets):
        """
        :param satellites: set of satellite UIDs
        """
        self.tasks_satellites = {s: [] for s in satellites}
        self.tasks_targets = {t: None for t in targets}
        self.tasks_all = set()

    def assign_task(self, tar,  sat, t, t_now):
        self.tasks_satellites[sat].append(
            {"target": tar,
             "time": t,
             "update": t_now
             }
        )

        self.tasks_targets[tar] = {
            "satellite": sat,
            "time": t,
            "update": t_now
        }

        self.tasks_all.add((tar, sat, t))

    def remove_task(self, tar, sat, t):
        for idx, x in enumerate(self.tasks_satellites[sat]):
            if x['target'] == tar and x['time'] == t:
                self.tasks_satellites[sat].pop(idx)

        self.tasks_targets[tar] = None

        self.tasks_all.remove((tar, sat, t))


def acquire_data(satellite, target, time, gateways):
    """
    Function representing the acquisition of a data packet by a satellite.
    A new packet is generated, using the target awaiting "capture" as the source
    :param satellite:
    :param target:
    :param time:
    :param gateways:
    :return:
    """
    generate_bundle(
        satellite,
        1,
        1,
        time,
        gateways,
        target.ttl,
        target.uid
    )

    # Reset the target so that it can be acquired by someone else
    target.post_acquire_reset(time)


def routing(cs, nodes):
    g = cs.dynamic_graph[0]
    print(cs.times[0])
    u_v_sets = list()
    delivered = []
    failed = []

    # for each node pair u, v with a connection
    for u, v, capacity_uv in g.edges(data='capacity'):
        # if one of the nodes in the contact is a target, ignore
        if not {u, v}.issubset(set(nodes.keys())):
            continue
        # if we've not already evaluated this node pair
        if {u, v} not in u_v_sets:
            # capacity from v-->u
            capacity_vu = g.edges[v, u]['capacity']
            u_v_sets.append({u, v})
            # if at least some capacity exists across one of the edges
            if any((capacity_uv, capacity_vu)):
                ttl_max = 0
                for node in (nodes[u], nodes[v]):
                    ttl_max = max(ttl_max, node.buffer.max_expire_time)
                if not ttl_max:
                    continue
                cs_spae = get_evolving_graph(cs, ttl_max)
                spae = Spae(nodes, u, v, cs_spae)

                # Carry out routing using the Spae routing algorithm
                delivered, failed = spae.route_packets()

    return delivered, failed


def simulation(times, targets, satellites, gateways, cs, mean_times):
    delivered_all = []
    failed_all = []
    nodes = {**satellites, **gateways}
    t_previous = 0
    tasking = Task(targets, satellites, gateways, cs)
    live_targets = PriorityQueue()
    task_table = TaskTable(satellites.keys(), targets.keys())
    requests = {}
    base_gateway_uid = [x.uid for x in gateways.values() if x.name == "Base Gateway"][0]

    # *** SIMULATION ***
    # For each time step, identify which targets are in need of acquisition and for
    # those that do not have a satellite assigned, carry out the tasking operation.
    # Then, check if any of the satellites have data acquisition tasks pending for that
    # time step and, if so, add the relevant packet to their buffer
    for t in times:
        time_elapsed = t - t_previous
        # Evaluate each target that isn't currently awaiting assignment (i.e.
        # isn't currently in a "live" state, to see if it has become "live" during the
        # most recent time step. If it has, create a request object and set the target
        # to "live"
        for target in targets.values():
            if not target.is_live and not target.assigned:
                if evaluate_event_geom(mean_times[target.uid], time_elapsed):
                    requests[target.uid] = Request(target.uid, 'deliver', t)
                    target.is_live = t
                    config.REQUEST_COUNT += 1
                    print(
                        'Request',
                        requests[target.uid].uid,
                        'has been made, total now',
                        config.REQUEST_COUNT
                    )

        # If the time happens to be one in which a new contact has been made
        if t == cs.times[0]:
            # Prior to a gateway-satellite contact, we must evaluate the set of targets,
            # identify which are "live", add them to the queue and evaluate them to find
            # the most suitable satellite for acquisition responsibility. There's no
            # value in evaluating this if we don't have a contact between a satellite
            # and gateway since there's nothing we can do with that information.
            if any(
                    [cs.dynamic_graph[0].out_degree[g] > 1 for g in gateways.keys()
                     if g != base_gateway_uid]
            ):
                # If the target has "become live" since the previous evaluation,
                # add it to the live_targets priority queue
                for target in targets.values():
                    if target.is_live:
                        live_targets.put((-(t-target.is_live), target.uid, target))

                # Extract each "live" target, one by one and assign a satellite and
                # acquisition time. Each assignation goes into the assigned table,
                # which is distributed throughout the network. The table contains the
                # time at which it was last updated
                while not live_targets.empty():
                    target = live_targets.get()[2]
                    target.is_live = False
                    # TODO add in some feature that highlights if a feasible task could
                    #  not be generated. This might happen if, for example not
                    #  acquisition event occurs before is necessary.
                    sat_time_acquire = tasking.assign(
                        target,
                        requests.pop(target.uid)
                    )
                    if sat_time_acquire:
                        sat_acquire = sat_time_acquire[0]
                        time_acquire = sat_time_acquire[1]
                        target.assigned = (sat_acquire, time_acquire)
                        task_table.assign_task(target.uid, sat_acquire, time_acquire, t)
                    else:
                        print('')


                # Attach the up to date task table to each gateway and update the
                # satellite's task table (if one's in view) so they're aligned
                for g in gateways.values():
                    if g.uid == base_gateway_uid:
                        continue
                    g.task_table = task_table
                    if cs.dynamic_graph[0].out_degree[g.uid] > 1:
                        for s in [
                            x for x in cs.dynamic_graph[0].adj[g.uid]
                            if x != base_gateway_uid
                        ]:
                            merge_task_tables(g, satellites[s], t)

            # Generate packets on board satellites that are in view of their target and
            # yet to "acquire" them
            # TODO Might be better here to keep log of which satellites have tasks,
            #  when, rather than always looping through every satellite and checking.
            #  If there are lots of satellites this is going to be inefficient.
            for s in satellites.values():
                for task in s.task_table.tasks_satellites[s.uid]:
                    if task['time'] == t:
                        # TODO it might not always be the case that data can be
                        #  acquired, this should be where we re-assign
                        acquire_data(s, targets[task['target']], t, set(gateways.keys()))
                        s.task_table.remove_task(task['target'], s.uid, t)

                # For each task that has an acquisition time that has passed, remove it
                to_remove = []
                for task in s.task_table.tasks_all:
                    if task[2] < t:
                        to_remove.append(task)
                for x in to_remove:
                    s.task_table.remove_task(x[0], x[1], x[2])

            delivered, failed = routing(cs, nodes)

            delivered_all += delivered

            failed_all += failed

            # Remove the static graph and time parameter related to the one that has just
            # been evaluated
            cs.dynamic_graph.pop(0)
            cs.times.pop(0)

            # If we have no more contact opportunities to evaluate, no point continuing
            if not cs.dynamic_graph:
                break

        t_previous = t

    return delivered_all, failed_all


def merge_task_tables(a, b, t_now):
    """
    Given two nodes in contact, update the task table so that they are each in
    possession of the latest network information and task assignment decisions
    :param a: Node 1 in the contact (either gateway or satellite object)
    :param b: Node 2 in the contact (always satellite)
    :return:
    """
    att = a.task_table
    btt = b.task_table
    # task items that are unique to A & B are extracted
    diff_a = att.tasks_all.difference(btt.tasks_all)
    diff_b = btt.tasks_all.difference(att.tasks_all)

    # For each task that is in A but not in B, check to see if B has a task assigned
    # for that target or not, and take the appropriate action
    for t in diff_a:
        target = t[0]
        spacecraft = t[1]
        acquire = t[2]
        a_version = t

        # If B does not have any task assigned for that target, assign this task as it
        # is more up to date
        if not btt.tasks_targets.get(target):
            btt.assign_task(target, spacecraft, acquire, t_now)

        # Otherwise, the task assigned for this target on B must be different to that
        # on A, so make sure both nodes have the most up to date one. Basically if the
        # task on A was updated more recently than the one on B, replace the one on B
        # with the one from A, otherwise, do the opposite
        else:
            b_version = (
                target,
                btt.tasks_targets[target]['satellite'],
                btt.tasks_targets[target]['time']
            )
            if att.tasks_targets[target]['update'] > btt.tasks_targets[target]['update']:
                btt.remove_task(b_version[0], b_version[1], b_version[2])
                btt.assign_task(a_version[0], a_version[1], a_version[2], t_now)
            else:
                att.remove_task(a_version[0], a_version[1], a_version[2])
                att.assign_task(b_version[0], b_version[1], b_version[2], t_now)

    for t in diff_b:
        target = t[0]
        spacecraft = t[1]
        acquire = t[2]
        # If B does not have any task assigned for that target, assign this task as it
        # is more up to date
        if not att.tasks_targets.get(target):
            att.assign_task(target, spacecraft, acquire, t_now)


def setup_targets():
    # Prepare the target objects and put them into a dict
    targets = list()
    # targets.append(Target(0, 0, 0, 'Equator target', 0, "capture"))
    # targets.append(Target(45, 45, 0, '45 degree target', 0, "capture"))
    # targets.append(Target(0, 90, 0, 'Equatorial target', 0, 'deliver'))
    targets.append(Target(-90, 0, 0, 'S Pole target', 0, 'deliver'))

    # for _ in range(5):
    #     targets.append(
    #         Target(
    #             randint(-90, 90),
    #             randint(-180, 180),
    #             randint(0, 4000),
    #             None,
    #             randint(0, 0),
    #             "deliver"
    #         )
    #     )

    return {t.uid: t for t in targets}


def setup_gateways():
    gateways = list()

    # Create a "Base" Gateway that will always connect to each of the other Gateway
    # objects but never connect to the satellites. This is then used as a central node
    # to which all data is to be delivered and from which all commands are generated.
    gateways.append(Gateway(0, 0, 0, "Base Gateway", 100))

    # Create the individual Gateway nodes to which satellites are intermittently connected
    gateways.append(Gateway(90, 0, 0, 'North Pole gateway'))
    # gateways.append(Gateway(30, -60, 0, '30 degree gateway'))

    return {g.uid: g for g in gateways}


def setup_satellites(jd_start, duration, t_step):
    # Prepare the satellite objects and put them into a dict
    num_sats = 10
    sma_range = (6871000, 7271000)
    inc_range = (radians(89), radians(90))

    # Generate a random satellite network using the orbit ranges provided
    sma, inc, raan, ta = random_topology(num_sats, sma_range, inc_range)

    # Create all of the satellite objects and propagate their orbits
    satellites = {}
    for x in range(num_sats):
        # Instantiate the satellite class, define the initial orbit conditions,
        # propagate the orbit and add the satellite to the collection
        s = Spacecraft(1000, 10, 3, 1, 0, 1., False)
        ic = [sma[x], 0., inc[x], raan[x], 0., ta[x]]
        s.get_orbit(ic, 'coe', jd_start, duration, t_step)
        satellites[s.uid] = s

    print("Orbit propagation complete")
    return satellites


def target_mean_time(targets):
    """
    Defines the probability attributes associated with different targets being
    requested. E.g. a target with a low mean time will be requested more often
    :param targets:
    :return:
    """
    # For each target, set the mean_time between a target going dormant and a request
    # being made. This is used to help determine whether or not a request for a
    # specific target is made. E.g. if the mean time between requests is very large,
    # and a target has only recently been requested, the likelihood of it being
    # requested again will be small.
    mean_time = {}
    for t in targets:
        mean_time[t] = randint(10, 20)

    return mean_time


def evaluate_event_geom(mean_time, t_elapsed):
    """
    Define whether or not the target is awaiting acquisition. E.g. if a long period
    has elapsed since the last acquisition, the likelihood of another acquisition
    being requested will be high
    :param mean_time: Mean number of seconds between event happening
    :param t_elapsed: Amount of time passed since the last check was made
    :return:
    """
    p = geometric_cdf(1 / mean_time, t_elapsed)

    if random() < p:
        return True
    else:
        return False


def main():
    jd_start = 1.
    sim_duration = int(10 * 86400)
    sim_step_size = 10
    isl_range = 000000

    targets = setup_targets()
    satellites = setup_satellites(jd_start, sim_duration, sim_step_size)
    gateways = setup_gateways()

    mean_times = target_mean_time(targets.keys())

    for s in satellites.keys():
        satellites[s].task_table = TaskTable(
            satellites,
            [x for x in targets.keys()]
        )

    mission = Mission(jd_start, sim_duration, sim_step_size)
    # TODO (CL) I really don't like this, calling a method within a class and adding
    #  the result of that to the same class... euwww! Surely there's a better way.
    #  Should we just define the format that the gateways, satellites and targets must
    #  take and then put in something to highlight if they're wrong? Is having them as
    #  a property any better? That would mean there's a getter and setter for each one,
    #  which would be a lot more code and still just do the same thing
    mission.gateways = mission.add_nodes(gateways)
    mission.satellites = mission.add_nodes(satellites)
    mission.targets = mission.add_nodes(targets)

    all_times = [i for i in range(0, sim_duration, sim_step_size)]
    all_nodes = {**targets, **satellites, **gateways}

    mission.edge_schedule(all_times, isl_range)
    print("Contact edges found")

    mission.build_contact_schedule(all_nodes)
    print("Contact opportunities formatted")

    # create the contact schedule
    cs = init_contact_schedule(
        [x for x in mission.cs.keys()],
        all_nodes,
        mission.cs
    )
    print("Contact Schedule built")

    delivered, failed = simulation(
        all_times,
        targets,
        satellites,
        gateways,
        cs,
        mean_times
    )

    analysis_main(delivered, failed)


if __name__ == "__main__":
    cProfile.run('main()')
